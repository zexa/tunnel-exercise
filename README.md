# tunnel-exercise
Homework exercise to drop packets according to rules set in a config file.

For more info regarding the exercise itself see `resources/`.

## Requirements
* Unix based OS
* Rust compiler

## Build
```
cargo build --release
```

Afterwards, the binary will be available in `./target/release/tunnel-exercise`

## Testing
```
cargo test
```

## Usage
**Note**: Need sudo rights to create the tun device.

```
tunnel-exercise

USAGE:
    tunnel-exercise <RULES_FILE> <INTERFACE_NAME> <LOCAL_IP> <REMOTE_IP>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

ARGS:
    <RULES_FILE>        Custom rules file (i.e. resources/example.rules)
    <INTERFACE_NAME>    Interface name which will be created & used while tunnel-exercise is running (i.e. te0)
    <LOCAL_IP>          Local IpV4 which will be assigned to INTERFACE_NAME (i.e. 10.0.0.1
    <REMOTE_IP>         IpV4 into which tunnel-exercise should map requests into if a packet's source is LOCAL_IP
                        (i.e. 192.168.88.25)
```

## Notes
I've decided to stop working on the exercise from respect to the interviewer
and leave this piece of code as proof of my honest crack at it.

I attempted to work on the program by first starting it with sudo and then 
using the script provided in the email to create the necessary iptables rules. 

The script would successfully route all packets to my created tunnel device, 
but I noticed that only domains from my local network would come through.

```bash
➜  tunnel-exercise git:(create-tun) ✗ curl google.com -v
*   Trying 216.58.208.206:80...
*   Trying 2a00:1450:401b:800::200e:80...
* Immediate connect fail for 2a00:1450:401b:800::200e: Network is unreachable
```

```bash
➜  tunnel-exercise git:(create-tun) ✗ curl router.lan -v
*   Trying 192.168.0.1:80...
* Connected to router.lan (192.168.0.1) port 80 (#0)
> GET / HTTP/1.1
> Host: router.lan
> User-Agent: curl/7.73.0
> Accept: */*
>
```
