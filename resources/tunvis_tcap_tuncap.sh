#!/bin/bash

# This script sets up two way routing from locally generated traffic to
# your tunnel and then from that tunnel to the Internet.
# In addition to rate limiting your tunnel software should do these two
# things to support this configuration:
# - Packets' with source ip of <local> must change their source ip to
# <remote> - these are outgoing packets to internet
# - Packets' with destination ip <remote> must change their destination
# ip to <local> - these are returning packets from internet

CMD=$1

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run with root privileges" 
   exit 1
fi

setargs () {
    set -u
    TUN=$2
    LOCAL=$3
    REMOTE=$4
    ETH=$5
    set +u
}

if [ "$CMD" = "up" ]; then

    setargs $@
    echo "$CMD tun:{$TUN $LOCAL -> $REMOTE} -> eth:{$ETH}"
    
    DEFAULT=$(ip route show | grep 'default via' | awk '{print $3}')
    echo "Using '$DEFAULT' as default gateway"

    set -x
    
    # Enable Ip Forawding
    sysctl -w net.ipv4.ip_forward=1
    
    # Configure Tunnel
    ip addr add local $LOCAL peer $REMOTE dev $TUN
    ip link set up dev $TUN

    # Configure Routing Table
    ip route add 0.0.0.0/0 via $LOCAL

    ip rule add from $REMOTE/32 lookup 666
    ip route add 0.0.0.0/0 via $DEFAULT dev $ETH table 666

    # Configure Firewall
    iptables -A FORWARD -i $TUN -o $ETH -j ACCEPT
    iptables -A FORWARD -i $ETH -o $TUN -j ACCEPT
    iptables -t nat -A POSTROUTING -o $ETH -j MASQUERADE

elif [ "$CMD" = "down" ]; then
    setargs $@
    echo "$CMD tun:{$TUN $LOCAL -> $REMOTE} -> eth:{$ETH}"
    set -x

    # Configure Firewall
    iptables -D FORWARD -i $TUN -o $ETH -j ACCEPT;
    iptables -D FORWARD -i $ETH -o $TUN -j ACCEPT; 
    iptables -t nat -D POSTROUTING -o $ETH -j MASQUERADE

    # Configure Routing Table
    ip route del 0.0.0.0/0 via $LOCAL

    ip rule del from $REMOTE/32 lookup 666
    ip route del 0.0.0.0/0 dev $ETH table 666

    # Configure Tunnel
    ip addr del local $LOCAL peer $REMOTE dev $TUN
    ip link set down dev $TUN
else
    echo "usage: tuncap { up | down} <tun> <local> <remote> <eth>"
    echo "  configures/cleanups all routing rules needed for tunnel capture."
    echo "params:"
    echo "  tun - name of tunnel interface."
    echo "  local - ip address of tun for local machine."
    echo "  remote - ip address of tun for virtual remote (tunnel process needs to override local ip to this ip)."
    echo "  eth - name of the interface used to send packets to outside world (egz.: ethX, wplXXX)."
    
    exit 1
fi
