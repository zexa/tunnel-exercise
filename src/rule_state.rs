use std::ops::Add;
use std::time::Duration;

use crate::rule::Rule;
use crate::rule_type::RuleType;

pub struct RuleState {
    pub rule: Rule,
    pub state: RuleType,
}

impl RuleState {
    pub fn from_rule(rule: Rule) -> Self {
        let state = match rule.rule_type.clone() {
            RuleType::Time(_) => RuleType::Time(Duration::new(0, 0)),
            RuleType::Size(_) => RuleType::Size(0),
        };
        Self {
            rule: rule.clone(),
            state,
        }
    }

    #[allow(dead_code)]
    pub fn to_string(&self) -> String {
        format!(
            "{} | {}/{}",
            self.rule.text,
            match self.state {
                RuleType::Time(time) => time.as_secs().to_string(),
                RuleType::Size(size) => size.to_string(),
            },
            match self.rule.rule_type {
                RuleType::Time(time) => time.as_secs().to_string().add(" s"),
                RuleType::Size(size) => size.to_string().add(" B"),
            },
        )
    }
}

mod test {
    #[allow(unused_imports)]
    use anyhow::Result;
    #[allow(unused_imports)]
    use crate::rule::Rule;
    #[allow(unused_imports)]
    use crate::rule_state::RuleState;

    #[test]
    fn time_parses_correctly() -> Result<()> {
        assert_eq!(
            "94.142.241.111/32 2m | 0/120 s",
            RuleState::from_rule(Rule::from_str("94.142.241.111/32 2m")?).to_string()
        );

        Ok(())
    }

    #[test]
    fn size_parses_correctly() -> Result<()> {
        assert_eq!(
            "80.249.99.148/32 11mb | 0/11000000 B",
            RuleState::from_rule(Rule::from_str("80.249.99.148/32 11mb")?).to_string()
        );

        Ok(())
    }
}