use anyhow::Result;
use pnet::packet::ethernet::{EthernetPacket, EtherTypes};
// use pnet::packet::ip::{IpNextHeaderProtocol, IpNextHeaderProtocols};

#[allow(unused_imports)]
use pnet::packet::Packet;

use crate::rule_state::RuleState;

pub struct PacketGatekeeper {}

impl PacketGatekeeper {
    pub fn new() -> Self {
        Self {}
    }

    pub fn is_allowed(
        &self,
        _ethernet_packet: &EthernetPacket,
        _rule_states: &mut Vec<RuleState>
    ) -> Result<bool> {
        Ok(true)
    }

    // fn match_rule(&self, packet: &EthernetPacket) -> Option<Rule> {
    //
    // }

    pub fn packet_is_ipv4(packet: &EthernetPacket) -> bool {
        match packet.get_ethertype() {
            EtherTypes::Ipv4 => true,
            _ => false,
        }
    }

    // fn packet_is_tcp(&self, protocol: IpNextHeaderProtocol) -> bool {
    //     match protocol {
    //         IpNextHeaderProtocols::Tcp => true,
    //         _ => false,
    //     }
    // }
}
