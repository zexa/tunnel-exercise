use std::time::Duration;

use anyhow::{Context, Result};
use anyhow::bail;
use regex::Regex;

#[derive(PartialEq)]
#[derive(Debug)]
#[derive(Clone)]
pub enum RuleType {
    Time(Duration),
    Size(u64),
}

impl RuleType {
    pub fn from_str(s: &str) -> Result<Self> {
        let rule = r"(\d*)(\w*)";
        let captures = Regex::new(rule)
            .context(format!("Could not build regex rule \"{}\"", rule))?
            .captures(s)
            .context(format!("Could not match regex rule \"{}\" on \"{}\"", rule, s))?;
        let value = &captures[1]
            .parse::<u64>()
            .context(format!("Could not parse \"{}\" into a number", &captures[1]))?;
        let value_type = &captures[2];

        match value_type {
            "kb" => Ok(Self::Size(value * 1000)),
            "mb" => Ok(Self::Size(value * 1000000)),
            "gb" => Ok(Self::Size(value * 1000000000)),
            "s" => Ok(Self::Time(Duration::from_secs(*value))),
            "m" => Ok(Self::Time(Duration::from_secs(*value * 60))),
            "h" => Ok(Self::Time(Duration::from_secs(*value * 3600))),
            _ => bail!(format!("Could not parse rule type \"{}\"", s)),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use crate::rule_type::RuleType;

    #[test]
    fn size_parses_correctly() {
        assert_eq!(RuleType::from_str("1kb").unwrap(), RuleType::Size(1000));
        assert_eq!(RuleType::from_str("1mb").unwrap(), RuleType::Size(1000000));
        assert_eq!(RuleType::from_str("1gb").unwrap(), RuleType::Size(1000000000));
    }

    #[test]
    fn time_parses_correctly() {
        assert_eq!(RuleType::from_str("1s").unwrap(), RuleType::Time(Duration::new(1, 0)));
        assert_eq!(RuleType::from_str("1m").unwrap(), RuleType::Time(Duration::new(60, 0)));
        assert_eq!(RuleType::from_str("1h").unwrap(), RuleType::Time(Duration::new(3600, 0)));
    }

    #[test]
    fn returns_err_on_failure() {
        assert!(RuleType::from_str("?").is_err());
    }
}
