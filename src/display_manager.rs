// use anyhow::{Context, Result};
use console::Term;

// use crate::rule_state::RuleState;

pub struct DisplayManager {
    _display: Term,
}

impl DisplayManager {
    pub fn new(display: Term) -> Self {
        Self {
            _display: display,
        }
    }

    // pub fn refresh_rule_states(&self, states: &Vec<RuleState>) -> Result<()> {
    //     self.display.clear_screen().context("Could not clear screen")?;
    //     for state in states {
    //         self.display.write_line(&state.to_string()).context("Could not write line")?;
    //     }
    //
    //     self.display.write_line("").context("Could not write line")?;
    //
    //     Ok(())
    // }
}