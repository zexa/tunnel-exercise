use std::str::FromStr;

use anyhow::{Context, Result};
use ipnet::{IpNet, Ipv4Net};

use crate::rule_type::RuleType;

#[derive(Debug)]
#[derive(Clone)]
pub struct Rule {
    pub text: String,
    pub network: IpNet,
    pub rule_type: RuleType,
}

impl Rule {
    pub fn from_str(text: &str) -> Result<Rule> {
        let rule_split: Vec<&str> = text.split(" ").collect();
        let (network, rule_type) = (rule_split[0], rule_split[1]);

        Ok(Rule {
            text: text.to_string(),
            network: IpNet::V4(
                Ipv4Net::from_str(network)
                    .context(format!("Could not parse network \"{}\"", network))?
            ),
            rule_type: RuleType::from_str(rule_type)
                .context(format!("Could not parse rule type \"{}\"", rule_type))?,
        })
    }
}
