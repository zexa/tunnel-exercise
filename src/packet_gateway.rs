use anyhow::{bail, Context};
use anyhow::Result;
use pnet::datalink::{DataLinkReceiver, DataLinkSender};
use pnet::packet::ethernet::EthernetPacket;
use pnet::packet::Packet;
use pnet::packet::ipv4::*;

use crate::display_manager::DisplayManager;
use crate::packet_gatekeeper::PacketGatekeeper;
use crate::rule_state::RuleState;
use std::net::Ipv4Addr;

// We listen for any packets that appear in the receiver.
// Whenever we receive a packet, we build the packet onto an EthernetPacket, which is a pnet struct
// that makes packets easier to work with.
// Weather or not to accept a packet is decided by the PacketGatekeeper. Imagine a big angry bouncer
// near a club entrance holding a list of rules and checking each person if they match the dress
// code.
// If the PacketGatekeeper says the packets are okay, we send them on their way. Otherwise, well...
pub struct PacketGateway {
    receiver: Box<dyn DataLinkReceiver>,
    sender: Box<dyn DataLinkSender>,
    packet_gatekeeper: PacketGatekeeper,
    _display: DisplayManager,
    rule_states: Vec<RuleState>,
    _local_ipv4: Ipv4Addr,
    _remote_ipv4: Ipv4Addr,
}

impl PacketGateway {
    pub fn new(
        sender: Box<dyn DataLinkSender>,
        receiver: Box<dyn DataLinkReceiver>,
        packet_gatekeeper: PacketGatekeeper,
        display: DisplayManager,
        rule_states: Vec<RuleState>,
        local_ipv4: Ipv4Addr,
        remote_ipv4: Ipv4Addr,
    ) -> Self {
        Self {
            sender,
            receiver,
            packet_gatekeeper,
            _display: display,
            rule_states,
            _local_ipv4: local_ipv4,
            _remote_ipv4: remote_ipv4,
        }
    }

    #[allow(unreachable_code)]
    pub fn listen(&mut self) -> Result<()> {
        loop {
            let packet = match self.receiver.next() {
                Err(e) => bail!(format!("Unable to receive packet: {}", e)),
                Ok(packet_bytes) => packet_bytes,
            };
            let packet = packet.to_owned();
            let ethernet_packet = EthernetPacket::new(packet.as_slice())
                .unwrap();

            // Rules for non ipv4 are not supported so we send the packets away
            if !PacketGatekeeper::packet_is_ipv4(&ethernet_packet) {
                self.sender.send_to(ethernet_packet.packet(), None);
            }

            let _ipv4_packet = MutableIpv4Packet::new(
                ethernet_packet.packet().to_owned().as_mut_slice()).unwrap();

            let decision = self.packet_gatekeeper
                .is_allowed(&ethernet_packet, &mut self.rule_states)
                .context("Could not decide what do to with packet")?;
            match decision {
                true => {
                    self.sender.send_to(ethernet_packet.packet(), None)
                },
                false => continue,
            };
        };

        Ok(())
    }

    // fn send(&mut self, _packet: &EthernetPacket) -> Result<()> {
    //     unimplemented!("Sending packets not yet implemented");
    // }
}
