// Error handling
extern crate anyhow;
// Eye sugar & argument parsing
extern crate clap;
// Displaying rule state
extern crate console;
// Parsing ipnets from strings & easy checks to see if ip belongs in ipnet
extern crate ipnet;
// Parsing packets & manipulating them
extern crate pnet;
// Parsing rules
extern crate regex;
// Creating tun device
extern crate tun;

use std::fs::read_to_string;

use anyhow::{Context, Result};
use anyhow::bail;
use clap::App;
use clap::AppSettings;
use clap::Arg;
use console::Term;
use pnet::datalink;
use pnet::datalink::Channel::Ethernet;

use crate::display_manager::DisplayManager;
use crate::packet_gatekeeper::PacketGatekeeper;
use crate::packet_gateway::PacketGateway;
use crate::rule::Rule;
use crate::rule_state::RuleState;
use std::net::Ipv4Addr;
use std::str::FromStr;

mod rule;
mod rule_type;
mod packet_gateway;
mod packet_gatekeeper;
mod rule_state;
mod display_manager;

// As per exercise requirements, we expect a few arguments, make sure that they're provided and
// panic if they're not.
// We parse the rules into comfortable structs to make them easier to work with.
// We create a tunnel with a name specified in the arguments. tun_tap handles removing the tunnel
// when the memory is cleared, that will be as soon as we exit from main.
// TODO: We create rules ensuring that all traffic will be sent to our tunnel.
// We create a communication channel with above tunnel, and pass the receiver and sender handlers to
// the gateway, which is responsible for either sending or dropping the packets.
fn main() -> Result<()> {
    let argument_matches = App::new("tunnel-exercise")
        .arg(Arg::with_name("rules")
            .value_name("RULES_FILE")
            .help("Custom rules file (i.e. resources/example.rules)")
            .takes_value(true)
            .required(true))
        .arg(Arg::with_name("interface")
            .value_name("INTERFACE_NAME")
            .help("Interface name which will be created & used while tunnel-exercise is running (i.e. te0)")
            .takes_value(true)
            .required(true))
        .arg(Arg::with_name("local_ip")
            .value_name("LOCAL_IP")
            .help("Local IpV4 which will be assigned to INTERFACE_NAME (i.e. 10.0.0.1")
            .takes_value(true)
            .required(true))
        .arg(Arg::with_name("remote_ip")
            .value_name("REMOTE_IP")
            .help("IpV4 into which tunnel-exercise should map requests into if a packet's source is LOCAL_IP (i.e. 192.168.88.25)")
            .takes_value(true)
            .required(true))
        .setting(AppSettings::ArgRequiredElseHelp)
        .get_matches();

    let rules_file = argument_matches
        .value_of("rules")
        .context("Missing required RULES_FILE argument")?;

    let rule_states = read_to_string(rules_file)
        .context(format!("Could not read rules file \"{}\"", rules_file))?
        .lines()
        .rev()
        .map(|line| {
            Rule::from_str(line)
                .context(format!("Could not parse \"{}\" into a rule", line))
                .unwrap()
        })
        .map(|rule| RuleState::from_rule(rule))
        .collect::<Vec<RuleState>>();

    let interface_name = argument_matches
        .value_of("interface")
        .context("Missing required INTERFACE_NAME argument")?;
    let _tun = tun::create(
        tun::Configuration::default()
            .name(&interface_name)
            .up()
    )
        .context(format!("Could not create device \"{}\"", interface_name))?;
    let interface = pnet::datalink::interfaces()
        .into_iter()
        .find(|interface| interface.name == interface_name)
        .context(format!("Could not find interface \"{}\"", interface_name))?;

    let channel = datalink::channel(&interface, Default::default());
    let (sender, receiver) = match channel {
        Ok(Ethernet(sender, receiver)) => (sender, receiver),
        Ok(_) => bail!("Unhandled channel type"),
        Err(e) => bail!(format!("Unable to create channel: {}", e)),
    };

    let local_ip = argument_matches
        .value_of("local_ip")
        .context("Missing required LOCAL_IP argument")?;
    let local_ipv4 = Ipv4Addr::from_str(local_ip)
        .context("Could not parse LOCAL_IP")?;

    let remote_ip = argument_matches
        .value_of("remote_ip")
        .context("Missing required REMOTE_IP argument")?;
    let remote_ipv4 = Ipv4Addr::from_str(remote_ip)
        .context("Could not parse REMOTE_IP")?;

    PacketGateway::new(
        sender,
        receiver,
        PacketGatekeeper::new(),
        DisplayManager::new(Term::stdout()),
        rule_states,
        local_ipv4,
        remote_ipv4,
    )
        .listen()
        .context("Failed to listen to packets")?;

    Ok(())
}
